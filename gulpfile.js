var gulp = require('gulp'),
	connect = require('gulp-connect'),
	concat = require("gulp-concat"),
	bower = require('main-bower-files'),
	less = require('gulp-less');

gulp.task('connect', function(){
	connect.server({
		livereload : true
	});
});
gulp.task('reload', function(){
	gulp.src('index.html')
		.pipe(connect.reload());
});
gulp.task('bower:js', function(){
	return gulp.src(bower('**/*.js'))
			   .pipe(concat('bower.js'))
			   //.pipe();
			   .pipe(gulp.dest('js/'));
});
gulp.task('bower:css', function(){
	return gulp.src(bower('**/*.css'))
			   .pipe(concat('bower.css'))
			   //.pipe();
			   .pipe(gulp.dest('css/'));
});
gulp.task('bower:less', function(){
	return gulp.src(bower('**/*.less'))
			   .pipe(concat('bower.less'))
			   //.pipe();
			   .pipe(gulp.dest('css/'));
});
gulp.task('bootstrap-less', function(){
	gulp.src('bower_components/bootstrap/less/bootstrap.less')
		.pipe(less())
		.pipe(gulp.dest('css/'));
});
gulp.task('watch', function(){
	gulp.watch(['./js/**/*.js', 'views/*.html', './*.html', '**/*.less', '!./js/index.js'], ['reload']);
});

gulp.task('default', ['connect', 'watch']);
gulp.task('bower', ['bower:js', 'bower:css']);
gulp.task('less', ['bootstrap-less']);