'use strict';

var app = angular.module('app', ['ngRoute']);
app.config(function($routeProvider, $locationProvider){
	$locationProvider.hashPrefix('');
	$routeProvider
	.when("/", {
		templateUrl : "views/home.html"
	})
	.when('/form', {
		templateUrl : "views/form.html"
	})
	.when('/events', {
		templateUrl : "views/events.html"
	})
	.when('/filters', {
		templateUrl : 'views/filters.html'
	})
	.when('/tabs', {
		templateUrl : 'views/tabs.html'
	})
	.otherwise({
		redirectTo : '/'
	});
})
.controller('pageController', ['$scope', function($scope){
	$scope.greeting = "Hola";
}])
.controller('homeController', ['$scope', '$window', function($scope, $window){
	$scope.myName = "Angular";
}])
.controller('formController', ['$scope', function($scope){
	$scope.onSubmit = function(){
		console.log($scope.user);
	}
}])
.controller('eventsController', ['$scope', function($scope){
	$scope.events = ["ng-blur", "ng-change", "ng-click", "ng-copy", "ng-cut", "ng-dbclick", "ng-focus", "ng-keydown", "ng-keypress", "ng-keyup", "ng-mousedown", "ng-mouseenter", "ng-mouseleave", "ng-mousemove", "ng-mouseover", "ng-mouseup", "ng-paste"];
	$scope.search = "";
	$scope.count = 0;
}])
.controller('filtersController', ['$scope', function($scope){
	$scope.showContent = true;
	$scope.tab = 1;
}])
.controller('tabsController', ['$scope', function($scope){
	$scope.tab = 1;
}])
.directive('menu', function(){
	return {
		restrict : 'E',
		templateUrl : 'views/menu.html',
		replace : true
	}
})
.filter('contains', function(){
	return function(arr, search){
		return arr.filter(function(item){
			return item.indexOf(search) > -1;
		});
	}
});